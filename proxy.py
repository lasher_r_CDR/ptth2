import socket
import select
import sys
import uuid
from pprint import pprint
import http
from client import Client

import pdb

class SimpleProxy:

    # dev
    dev = True
    verbose = True

    def __init__(self, addrIn, addrOut, dattaAddr):
        self.addrIn = addrIn
        self.addrOut = addrOut

        self._sockBrowsers = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sockBrowsers.bind(addrIn)

        self._sockControl = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sockControl.bind(addrOut)

        self._sockData = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sockData.bind(dattaAddr)

    def start(self):
        self._sockBrowsers.listen(5)
        self._sockControl.listen(5)
        self._sockData.listen(5)

        dataClients = []
        browserClients = []
        controll = None
        running = 1

        clientObjs = []

        if self.verbose:
            print "starting"

        #used by select() to wait for new clients and keyboard input
        selInput = [self._sockData, self._sockBrowsers, self._sockControl] 

        while running:
            try:
                inReady, outReady, error = select.select(selInput, [], [])

                for eventOrig in inReady:

                    if eventOrig == self._sockData:
                        newDataClient, address = self._sockData.accept()
                        if self.verbose:
                            print 'new data conn from ', address
                        selInput.append(newDataClient)
                        dataClients.append(newDataClient)

                    if eventOrig == self._sockBrowsers:
                        '''
                        sock input event means a new client is trying to contact the server. 
                        The server calls accept() to get the new client and then appends the client to its list of input sockets. 
                        This means that next time it calls select() it can handle any input the client has sent on this new socket.
                        '''

                        newClient, address = self._sockBrowsers.accept()

                        if self.verbose:
                            print "new connection from ", address
                        selInput.append(newClient)
                        browserClients.append(newClient)
                        cObj = Client(str(uuid.uuid1()), newClient)
                        clientObjs.append(cObj)

                    elif eventOrig == self._sockControl:
                        '''
                        new control connection
                        '''
                        newControlCon, address = self._sockControl.accept()

                        if self.verbose:
                            print "new control conn"
                        selInput.append(newControlCon)
                        controll = newControlCon

                    elif eventOrig == controll:
                        '''
                        data is comming from controll,
                        verify control conn
                        send ok
                        '''

                    elif eventOrig in dataClients:
                        '''
                        decode data from ptth response to http response
                        get the right browser client
                        send decoded resp to right browser client
                        '''

                        data = eventOrig.recv(1024)
                        if data:
                            if self.verbose:
                                print "receved data from agent: ", data
                            for browserClient in browserClients:#todo this should send to correct browser not all
                                browserClient.send(data)
                                eventOrig.send('HTTP/1.1 200 OK\r\n\r\n')
                        else:
                            print "data conn close"
                            eventOrig.close()
                            dataClients.remove(eventOrig)
                            selInput.remove(eventOrig)

                    elif eventOrig in browserClients:
                        '''
                        data comming from browser,
                        encode into ptth request,
                        send to agent over control
                        '''
                        data = eventOrig.recv(1024*10)
                        if data:
                            if self.verbose:
                                print data
                            if controll:
                                '''
                                TODO: encode http request -> ptth request
                                send encoded ddata
                                '''
                                for client in clientObjs:
                                    if eventOrig == client.bConn:
                                        print "found the right client obj"
                                        httpRequest = client.requestFromString(data)
                                        import pdb; pdb.set_trace()
                                        print httpRequest.encToPtth()
                                #req = http.requestFromString(data)
                                #req.addHeader('A_ID', str(uuid.uuid1()));
                                #pprint(vars(req))
                                #pdb.set_trace()
                                #controll.send(data)
                        else:
                            eventOrig.close()
                            browserClients.remove(eventOrig)
                            selInput.remove(eventOrig)
                            if self.verbose:
                                print "browser conn clossed"
            except KeyboardInterrupt:
                print 'keyboard interrup'
                self._sockControl.close()
                self._sockData.close()
                self._sockBrowsers.close()
                break

if __name__ == "__main__":
    server = SimpleProxy(('',8081), ('', 8082), ('', 8083))
    # browser, control, data 
    server.start()

