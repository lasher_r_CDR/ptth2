import re
import requests
from urlparse import urlparse
import pdb
from pprint import pprint

class Request:

    def __init__(self, bId):
        self.bId = bId
        pass

    def setReqParams(self, metthod, dest, headers):
        reg = r"([https?]+://)?([^\s^/]+)(.+)?"
        self.method = metthod
        m = re.match(reg, dest)
        self.scheme = m.group(1) if m.group(1) else 'http'
        self.host = m.group(2) if m.group(2) else self.host
        self.path = m.group(3) if m.group(3) else ''
        self.dest = self.scheme+'://'+self.host+self.path
        self.headers = headers
        pass

    def send(self):
        print self.method
        if self.method == 'GET':
            try:
                pdb.set_trace()
                r = requests.get(self.dest, headers=self.headers)
                return r.content
            except requests.exceptions.ConnectionError as e:
                print e.strerror
                return None

    def addHeader(self, key, val):
        print key

    def encToPtth(self):
        status = 'HTTP:/1.1 200 OK'
        content = self.method + ' ' + self.dest + ' ' + 'HTTP/1.1'
        bidHeader = 'Browser-ID: '+self.bId
        cookies = 'Cookie: ' + self.headers['Cookie'];
        contentLengthHeader = 'Content-Length: '+str(len(content))
        line = '\r\n'
        return status + line + bidHeader + line + cookies +line + contentLengthHeader + line + line + content + line + line

def requestFromString(requestStr, bId):
    lines = requestStr.split('\n')

    firstLine = lines.pop(0)

    headersDict = dict()
    for line in lines:
        if line == '\r':
            break
        headerParts = line.split(':', 1)
        headerKey = headerParts[0]
        headerVal = headerParts[1].replace('\r','').lstrip()
        headersDict[headerKey] = headerVal

    reg = r'(\w+) /(.+) '
    match = re.match(reg, firstLine)
    method = match.group(1)
    dest = match.group(2)

    request = Request(bId)
    request.setReqParams(method, dest, headersDict)
    return request

def responsFromRequestObj(obj):
    pass
    pdb.set_trace()
    print 'need break point'
